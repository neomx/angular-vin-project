import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { OffersPage } from '../offers/offers';
import { VehiclesPage } from '../vehicles/vehicles';
import { ProfilePage } from '../profile/profile';
/*import { LaunchPage } from '../launch/launch';*/

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  homePage = HomePage;
  offersPage = OffersPage;
  vehiclesPage = VehiclesPage;
  profilePage = ProfilePage;
  /*launchPage = LaunchPage;*/
  public tabIndex:Number = 0;

  constructor(public params:NavParams) {
    let tabIndex = this.params.get('tabIndex');
    if(tabIndex){
      this.tabIndex = tabIndex;
    }
    console.log(tabIndex);
  }

}
