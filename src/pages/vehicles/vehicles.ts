import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';
import {GlobalFunctions} from "../../providers/global-functions";

import { ScanPage } from "../scan/scan";
import { ManageListingPage } from './../manage-listing/manage-listing';
import { ListingsDataProvider } from './../../providers/data/listings-data';

@IonicPage()
@Component({
  selector: 'page-vehicles',
  templateUrl: 'vehicles.html',
})
export class VehiclesPage {

  myListings: any;
  isDefaultVisible: boolean = true;
  listingDetail = ManageListingPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public listingsDataProvider: ListingsDataProvider, public globalFunctions: GlobalFunctions, private consumerAPIProvider: ConsumerAPIProvider) {
    this.getMyListings();
  }


  //This function is the one that is hooked up to the dummy JSON file..
  getMyListings() {
    this.globalFunctions.showLoader('Finding your listings!');
    this.listingsDataProvider.getMyListings().then( data => {
        console.log(data);
        this.myListings = data ? data : null;
        this.isDefaultVisible = this.myListings.tradeIns.length === 0 ? true : false;
        this.myListings = this.myListings.tradeIns;
        this.globalFunctions.loading.dismiss();
        this.globalFunctions.presentToast('Here are your listings!')
      },
      err => {
        this.globalFunctions.loading.dismiss();
        this.globalFunctions.presentToast(err);
      });
  }


  //This function is the one that is hooked up to the API
  // getMyListings() {
  //   this.globalFunctions.showLoader('Finding your listings!');
  //   this.consumerAPIProvider.doGet('/tradeIn/list/').subscribe( data => {
  //     console.log(data);
  //     this.myListings = data ? data : null;
  //     this.isDefaultVisible = this.myListings.tradeIns.length === 0 ? true : false;
  //     this.myListings = this.myListings.tradeIns;
  //     this.globalFunctions.loading.dismiss();
  //     this.globalFunctions.presentToast('Here are your listings!')
  //   },
  //   err => {
  //     this.globalFunctions.loading.dismiss();
  //     this.globalFunctions.presentToast(err);
  //   });
  // }

  updateSelectedListing(listing) {
    this.listingsDataProvider.updateSelectedListing(listing);
  }

  doPush() {
    this.navCtrl.push(ScanPage);
  }

}
