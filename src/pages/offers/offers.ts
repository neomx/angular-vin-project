import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { OffersDataProvider } from '../../providers/data/offers-data';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';

import { OfferDetailPage } from './../offer-detail/offer-detail';
import { OfferAcceptedPage } from '../offer-accepted/offer-accepted';
import { OfferDeclinedPage } from "../offer-declined/offer-declined";
import {GlobalFunctions} from "../../providers/global-functions";

@IonicPage()
@Component({
  selector: 'page-offers',
  templateUrl: 'offers.html',
})
export class OffersPage {
  myoffers: any;
  isDefaultVisible: boolean = true;


  constructor(public navCtrl: NavController, public navParams: NavParams, public offersDataProvider: OffersDataProvider,public globalFunctions: GlobalFunctions, private consumerAPIProvider: ConsumerAPIProvider, public alertCtrl: AlertController) {
    this.getMyOffers();
  }

  //this function is the one that is hooked up the dummy json API
  getMyOffers() {
    this.globalFunctions.showLoader('Getting Your Offers!');
    this.offersDataProvider.getMyOffers().then(data => {
        this.globalFunctions.loading.dismiss();
        this.globalFunctions.presentToast('Here are your offers!');
        this.myoffers = data ? data : null;
        this.isDefaultVisible = this.myoffers.length === 0 ? true : false;
        this.sortMyOffers(this.myoffers);
      },
      err => {
        this.globalFunctions.loading.dismiss();
        this.globalFunctions.presentToast(err);
      });
  }

  //This function is the one that is hooked up to the API
  // getMyOffers() {
  //   this.globalFunctions.showLoader('Getting Your Offers!');
  //   this.consumerAPIProvider.doGet('/offer/list?extended=true').subscribe(data => {
  //     this.myoffers = data ? data : null;
  //     this.myoffers = this.myoffers.offers;
  //     this.isDefaultVisible = this.myoffers.length === 0 ? true : false;
  //     this.sortMyOffers(this.myoffers);
  //     console.log(this.myoffers);
  //     this.globalFunctions.loading.dismiss();
  //     this.globalFunctions.presentToast('Here are your offers!');
  //   },
  //   err => {
  //     this.globalFunctions.loading.dismiss();
  //     this.globalFunctions.presentToast(err);
  //   });
  // }

  sortMyOffers(data) {
    data.sort(function(a, b) {
      if ((a.read === false) && (b.read === true)) {
        return -1;
      } else if ((a.read === true) && (b.read === false)) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  updatedSelectedOffer(offer) {
    this.offersDataProvider.updateSelectedOffer(offer);
    this.navCtrl.push(OfferDetailPage);
  }

  showConfirm(offer) {
    this.offersDataProvider.updateSelectedOffer(offer);
    let confirm = this.alertCtrl.create({
      title: 'Offer',
      message: 'Do you want to accept this offer?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('cancel clicked');
          }
        },
        {
          text: 'Accept',
          handler: () => {
            this.navCtrl.push(OfferAcceptedPage);
          }
        }
      ]
    });
    confirm.present();
  }

  showDecline(offer) {
    this.offersDataProvider.updateSelectedOffer(offer);
    let decline = this.alertCtrl.create({
      title: 'Offer',
      message: 'Do you want to decline this offer?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('cancel clicked');
          }
        },
        {
          text: 'Decline',
          handler: () => {
            this.navCtrl.push(OfferDeclinedPage);
          }
        }
      ]
    });
    decline.present();
  }

  offerDifference(offer) {
    return offer.price - offer.tradeIn.price;
  }
}
