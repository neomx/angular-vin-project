import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehiclePricingPage } from './vehicle-pricing';

@NgModule({
  declarations: [
    VehiclePricingPage,
  ],
  imports: [
    IonicPageModule.forChild(VehiclePricingPage),
  ],
})
export class VehiclePricingPageModule {}
