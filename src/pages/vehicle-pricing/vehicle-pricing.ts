import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VehiclePostCreatePage } from '../../pages/vehicle-post-create/vehicle-post-create';
import { ProgressBarModule } from "../progress-bar/progress-bar.module";

@IonicPage()
@Component({
  selector: 'page-vehicle-pricing',
  templateUrl: 'vehicle-pricing.html',
})
export class VehiclePricingPage {
  vehDesc: any;

  progressBarWidth: number = 70;
  startBackgroundColor: string = "#43B7E8";
  iconColor: string = "#43B7E8";
  startBorderColor: string = "2px solid #43B7E8";


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.vehDesc = navParams.get('vehDesc');
  }

  doPush(){
    this.navCtrl.push(VehiclePostCreatePage, {vehDesc: this.vehDesc});
  }

  ionViewDidLoad() {
  }

}
