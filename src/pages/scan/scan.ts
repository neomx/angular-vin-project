import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Toast } from '@ionic-native/toast';
import { UserProvider } from '../../providers/user/user';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';
import { GlobalFunctions } from '../../providers/global-functions';
import { GlobalVars } from '../../providers/global-vars';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { NoScanPage } from '../../pages/no-scan/no-scan';
import { VehicleTrimPage } from '../../pages/vehicle-trim/vehicle-trim';

@IonicPage()
@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
})
export class ScanPage {
  noScanPage = NoScanPage;
  vehicleTrimPage = VehicleTrimPage;
  private api: any;
  trimResult: any;
  jsonText: any;
  private count: any;
  apiResult: any;
  trimData = { 'trims': [] };
  public clientVehObj: any;

  constructor(public navCtrl: NavController, private barcodeScanner: BarcodeScanner, private toast: Toast,
     private app: App, private consumerAPIProvider: ConsumerAPIProvider,public globalFunctions: GlobalFunctions,
     private globalVars: GlobalVars, private screenOrientation: ScreenOrientation) {
    }

  /*
  scan() {
    this.barcodeScanner.scan({formats: 'CODE_128, CODE_39, DATA_MATRIX, QR_CODE, EAN_13'}).then((barcodeData) => {
      this.toast.show('format: ' + barcodeData.format + ' data: ' + barcodeData.text,'5000','center').subscribe(toast => { console.log(toast);})
    });
  }
  */

  scanVIN() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    this.barcodeScanner.scan({formats: 'CODE_128, CODE_39, DATA_MATRIX, QR_CODE, EAN_13'}).then((barcodeData) => {
      if (barcodeData.text.length > 0)
      {
        this.doDecodeVIN(barcodeData.text);
      }
      else
      {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      }
    });
  }

  noScan(){
    this.navCtrl.push(NoScanPage);
  }

  doDecodeVIN(barcode){
    let vehID;
    let vehYear = '';
    let vehMake = '';
    let vehModel = '';
    let vehVIN = '';
    let vehDesc = '';
    let vehBody = '';
    let vehDriveTrain = '';
    this.globalFunctions.showLoader('Decoding...');
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    let vin = barcode;
    this.api = this.consumerAPIProvider
      .doGet('/decode/'+vin)
      .subscribe(
        result => {
          this.apiResult = JSON.stringify(result);
          let jsonText = JSON.parse(this.apiResult);
          this.count = jsonText.vinDecode.trims.length;
          for (var i = 0; i<this.count; i++){
            this.trimData.trims.push({'id': jsonText.vinDecode.trims[i].id,'name': jsonText.vinDecode.trims[i].name});
          }

          let jsonKeyVal = JSON.parse(this.apiResult, (key, value) => {
            //console.log('keyval:' + key+':'+value);
            if (key === 'vehicleId')
            {
              vehID = value;
            }
            if (key === 'year')
            {
              vehYear = value;
            }
            if (key === 'make')
            {
              vehMake =  value;
            }
            if (key === 'model')
            {
              vehModel = value;
            }
            if (key === 'vin')
            {
              vehVIN = value;
            }
            if (key === 'body')
            {
              vehBody = value;
            }
            if (key === 'drivetrain')
            {
              vehDriveTrain = value;
            }
          }
        )},
        err => {
          this.globalFunctions.loading.dismiss();
          this.globalFunctions.presentToast(err);
        },
        () => {
          this.api.unsubscribe();
          this.globalFunctions.loading.dismiss();
          this.globalFunctions.presentToast('Decoding completed.');
          if (this.count > 0)
          {
            this.globalVars.clientVehicleObj.vin.push(vehVIN);
            this.globalVars.clientVehicleObj.year.push(vehYear);
            this.globalVars.clientVehicleObj.make.push(vehMake);
            this.globalVars.clientVehicleObj.model.push(vehModel);
            this.globalVars.clientVehicleObj.bodyType.push(vehBody);
            this.globalVars.clientVehicleObj.drivetrain.push(vehDriveTrain);
            this.globalVars.storage3 = this.globalVars.clientVehicleObj;

            this.globalVars.storage = this.trimData.trims;
            vehDesc = vehYear+' '+vehMake+' '+vehModel;
            
            this.clientVehObj.extras.vehicleId = vehID;
            this.clientVehObj.vin = vehVIN;
            this.clientVehObj.year = vehYear;
            this.clientVehObj.make = vehMake;
            this.clientVehObj.model = vehModel;
            this.clientVehObj.vin = vehBody;
            this.clientVehObj.drivetrain= vehDriveTrain;
            this.clientVehObj.body= vehBody;
            this.globalVars.storage1 = this.clientVehObj;

            this.navCtrl.push(VehicleTrimPage, {vehVIN: vehVIN, vehDesc: vehDesc});
          }
          else
          {
            this.globalFunctions.presentToast('No VIN data was found.');
          }
        }
      );
  }

  ngOnInit(){
    console.log("ngOnInit called");
    this.clientVehObj = this.globalVars.storage1;
  }

  ngOnDestroy(){
    console.log("ngOnDestroy called from scan.ts");
    //if (this.api.subscribed) {
    //  this.api.unsubscribe();
    //}
  }

}
