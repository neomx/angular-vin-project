import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleOptionsPage } from './vehicle-options';

@NgModule({
  declarations: [
    VehicleOptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleOptionsPage),
  ],
})
export class VehicleOptionsPageModule {}
