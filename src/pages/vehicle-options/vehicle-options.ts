import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { ProgressBarModule } from "../progress-bar/progress-bar.module";
import { VehicleOdometerPage } from '../vehicle-odometer/vehicle-odometer';

@IonicPage()
@Component({
  selector: 'page-vehicle-options',
  templateUrl: 'vehicle-options.html',
})
export class VehicleOptionsPage {
  optionsData: any;
  isSelected: boolean = false;
  public noOptions;
  vehDesc: any;
  public clientVehObj: any;
  private options = [];

  progressBarWidth: number = 18;
  startBackgroundColor: string = "#43B7E8";
  startBorderColor: string = "2px solid #43B7E8";

  constructor(public navCtrl: NavController, public navParams: NavParams, private globalVars: GlobalVars) {
    this.vehDesc = navParams.get('vehDesc');
    this.noOptions = navParams.get("noOptionsFlag");
  }

  ngOnInit(){
    this.optionsData = this.globalVars.storage2.map(option => {
      option.isSelected = false;
      return option;
    });
    this.globalVars.clientVehicleObj = this.globalVars.storage3;
    this.clientVehObj = this.globalVars.storage1;
  }

  doSelect(selected: any){
    selected.isSelected = !selected.isSelected;
    this.options.push(selected.value);
  }

  doPush(){
    this.clientVehObj.options = this.options;
    this.globalVars.storage1 = this.clientVehObj;
    this.navCtrl.push(VehicleOdometerPage, {vehDesc: this.vehDesc});
  }
}
