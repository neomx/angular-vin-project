import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleIntColorPage } from './vehicle-int-color';

@NgModule({
  declarations: [
    VehicleIntColorPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleIntColorPage),
  ],
})
export class VehicleIntColorPageModule {}
