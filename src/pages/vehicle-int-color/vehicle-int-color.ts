import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';
import { GlobalFunctions } from '../../providers/global-functions';
import { GlobalVars } from '../../providers/global-vars';
import { VehicleOptionsPage } from '../../pages/vehicle-options/vehicle-options';

import { ProgressBarModule } from "../progress-bar/progress-bar.module";

@IonicPage()
@Component({
  selector: 'page-vehicle-int-color',
  templateUrl: 'vehicle-int-color.html',
})
export class VehicleIntColorPage {
  vehTrim: any;
  vehVIN: any;
  vehDesc: any;
  private api: any;
  intColorsResult: any;
  intColorsAry: any[];
  private count: any;
  private jsonText: any;
  public noIntColors;
  public noOptions;
  public clientVehObj: any;

  progressBarWidth: number = 14;
  startBackgroundColor: string = "#43B7E8";
  circleColor: string = "2px solid #43B7E8";

  constructor(public navCtrl: NavController, public navParams: NavParams, private consumerAPIProvider: ConsumerAPIProvider, public globalFunctions: GlobalFunctions, private globalVars: GlobalVars) {
    this.vehTrim = navParams.get('vehTrim');
    this.vehVIN = navParams.get('vehVIN');
    this.vehDesc = navParams.get('vehDesc');
    this.noIntColors = navParams.get('noIntColorsFlag');
    this.noOptions = navParams.get("noOptionsFlag");
    //console.log('clientvehobj:'+JSON.stringify(this.clientVehObj));
  }

  ngOnInit(){
    this.intColorsAry = this.globalVars.storage;
    this.globalVars.clientVehicleObj = this.globalVars.storage3;
    this.clientVehObj = this.globalVars.storage1;
  }

  doPush(selected: any){
    this.globalVars.storage = this.intColorsAry;
    this.globalVars.clientVehicleObj.colorInterior.push(selected);
    this.globalVars.storage3 = this.globalVars.clientVehicleObj;

    this.clientVehObj.colorInterior = selected.name;
    this.globalVars.storage1 = this.clientVehObj;

    this.navCtrl.push(VehicleOptionsPage, {vehDesc: this.vehDesc, noOptionsFlag: this.noOptions});
  }

  getColorCode(code) {
    return "#" + code;
  }
}
