import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleConditionsPage } from './vehicle-conditions';

@NgModule({
  declarations: [
    VehicleConditionsPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleConditionsPage),
  ],
})
export class VehicleConditionsPageModule {}
