import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VehiclePricingPage } from '../../pages/vehicle-pricing/vehicle-pricing';
import { ProgressBarModule } from "../progress-bar/progress-bar.module";
import { GlobalVars } from '../../providers/global-vars';

@IonicPage()
@Component({
  selector: 'page-vehicle-conditions',
  templateUrl: 'vehicle-conditions.html',
})
export class VehicleConditionsPage {
  vehDesc: any;
  conditionFormCompleted: {};
  public clientVehObj: any;

  progressBarWidth: number = 52;
  startBackgroundColor: string = "#43B7E8";
  iconColor: string = "#43B7E8";
  startBorderColor: string = "2px solid #43B7E8";

  public conditionForm = this.fb.group({
    title: ['', Validators.required],
    doesUserOweMoney: ['', Validators.required],
    isTitleClean: ['', Validators.required],
    doesVehicleRun: ['', Validators.required],
    accident: ['', Validators.required],
    bodyWork: ['', Validators.required],
    afterMarketParts: ['', Validators.required],
    odors: ['', Validators.required],
    keySets: ['', Validators.required],
    paintCondition: ['', Validators.required],
    dentsDings: ['', Validators.required],
    windowCondition: ['', Validators.required],
    lightCondition: ['', Validators.required],
    seatCondition: ['', Validators.required],
    carpetCondition: ['', Validators.required],
    cabinCondition: ['', Validators.required],
    tireCondition: ['', Validators.required],
    steeringCondition: ['', Validators.required],
    acHeatingCondition: ['', Validators.required],
    brakeCondition: ['', Validators.required],
    engineIssues: ['', Validators.required],
    transmissionIssues: ['', Validators.required],
    electricalIssues: ['', Validators.required],
    warningLight: ['', Validators.required],
    fluidLeaks: ['', Validators.required],
  });

  constructor(public navCtrl: NavController, public navParams: NavParams, public fb: FormBuilder, private globalVars: GlobalVars) {
    this.vehDesc = navParams.get('vehDesc');
  }

  ngOnInit(){
    this.clientVehObj = this.globalVars.storage1;
  }

  doPush() {
    this.conditionFormCompleted = this.conditionForm.value;
    this.navCtrl.push(VehiclePricingPage, {vehDesc: this.vehDesc});
  }

  ionViewDidLoad() {
  }

}
