import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Device } from '@ionic-native/device';
import { UsernameValidator } from '../../validators/username.validator';
import { PasswordValidator } from '../../validators/password.validator';
import { UserProvider } from '../../providers/user/user';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';
import { GlobalFunctions } from '../../providers/global-functions';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  validations_form: FormGroup;
  matching_passwords_group: FormGroup;
  loading: any;
  private api: any;

  signupData = { email: '', password:'',
    deviceInfo: {
      model: this.device.model,
      platform: this.device.platform,
      uuid: this.device.uuid,
      version: this.device.version,
      manufacturer: this.device.manufacturer,
      isVirtual: true,
      serial: this.device.serial,
      ip: '[:IP:]'
    }
  };

  /*
  signupData = { 
    username:'', 
    firstName: '',
    lastName: '',
    email: '',
    password:'',
    phoneCell: '',
    phoneHome: '',
    address: '',
    address2: '',
    postalCode: '',
    city: '',
    province: '',
  };
  */
  apps: any = {
    'Email' : [{
      name: 'Email'
    }],
    'CellPhone' : [{
      name: 'CellPhone'
    }],
    'HomePHone' : [{
      name: 'HomePhone'
    }]
  };
  data: any;
  
  constructor(public navCtrl: NavController, public userProvider: UserProvider, public loadingCtrl: LoadingController, private toastCtrl: ToastController,
     public formBuilder: FormBuilder, public consumerAPIProvider: ConsumerAPIProvider, public globalFunctions: GlobalFunctions, public device: Device) {}

  /*
  deviceData = {
    deviceInfo: {
      model: this.device.model,
      platform: this.device.platform,
      uuid: this.device.uuid,
      version: this.device.version,
      manufacturer: this.device.manufacturer,
      isVirtual: true,
      serial: this.device.serial,
      ip: '[:IP:]'
    }
  };
  */
  /*
  deviceData = {
    deviceInfo: {
      model: 'iPhone 8',
      platform: 'IOS',
      uuid: 'Test',
      version: 'Test',
      manufacturer: 'Apple',
      isVirtual: true,
      serial: 'Test',
      ip: '[:IP:]'
    }
  };
  */

  doSignup() {
    this.globalFunctions.showLoader('Creating new account...');
    this.api = this.consumerAPIProvider
      .doPost('/client', JSON.stringify(this.signupData))
      .subscribe(
        result => {
          this.globalFunctions.loading.dismiss();
        }, //eo result
        err => {
          this.globalFunctions.loading.dismiss();
          this.globalFunctions.presentToast(err);
        }, //eo err
        () => {
          this.globalFunctions.loading.dismiss();
          this.navCtrl.setRoot(TabsPage, {tabIndex: 0});
        }
      ) //eo subscribe
  };

  ionViewWillLoad(){
    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    }); /*eo FormGroup*/
    
    this.validations_form = this.formBuilder.group({
      name: new FormControl('', Validators.compose([
        UsernameValidator.validUsername,
        Validators.maxLength(25),
        Validators.minLength(5),
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
        Validators.required
      ])),
      username: new FormControl('', Validators.compose([
        UsernameValidator.validUsername,
        Validators.maxLength(15),
        Validators.minLength(5),
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      matching_passwords: this.matching_passwords_group,
      cellphone: new FormControl('', Validators.compose([
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      homephone: new FormControl('', Validators.compose([
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      zip: new FormControl('', Validators.compose([
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ]))
    }); /*eo formBuilder group*/
  } /*eo IonViewWillLoad*/

  validation_messages = {
    'username': [
      { type: 'required', message: 'Username is required.' },
      { type: 'minlength', message: 'Username must be at least 5 characters long.' },
      { type: 'maxlength', message: 'Username cannot be more than 15 characters long.' }
      //{ type: 'pattern', message: 'Your username must contain only numbers and letters.' },
      //{ type: 'validUsername', message: 'Your username has already been taken.' }
    ],
    'email': [
      { type: 'required', message: 'E-mail is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
      //{ type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirm password is required' }
    ],
    'matching_passwords': [
      { type: 'areEqual', message: 'Password mismatch' }
    ]
  };

}
