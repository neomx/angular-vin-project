import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VehiclePostConfirmPage } from '../../pages/vehicle-post-confirm/vehicle-post-confirm';
import { ProgressBarModule } from "../progress-bar/progress-bar.module";
import { GlobalVars } from '../../providers/global-vars';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';
import { GlobalFunctions } from '../../providers/global-functions';

@IonicPage()
@Component({
  selector: 'page-vehicle-post-create',
  templateUrl: 'vehicle-post-create.html',
})
export class VehiclePostCreatePage {
  vehDesc: any;
  public clientVehObj: any;
  private api: any;

  progressBarWidth: number = 90;
  startBackgroundColor: string = "#43B7E8";
  iconColor: string = "#43B7E8";
  startBorderColor: string = "2px solid #43B7E8";

  constructor(public navCtrl: NavController, public navParams: NavParams, private globalVars: GlobalVars,
     private consumerAPIProvider: ConsumerAPIProvider, private globalFunctions: GlobalFunctions) {
    this.vehDesc = navParams.get('vehDesc');
  }

  ngOnInit(){
    this.clientVehObj = this.globalVars.storage1;
  }

  doPost(){
    //push the last vin decode page, for demo purposes
    this.globalFunctions.presentToast('Your vehicle post was successful.');
    this.navCtrl.push(VehiclePostConfirmPage, {vehDesc: this.vehDesc});
    
    /*
    let vin = this.clientVehObj.extras.vehicleId;
    this.api = this.consumerAPIProvider
      .doPut('/clientVehicle/'+vin, JSON.stringify(this.clientVehObj))
      .subscribe(
        result => {
          console.log('result:'+result);
        },
        err => {
          this.globalFunctions.loading.dismiss();
          this.globalFunctions.presentToast(err);
        },
        () => {
          this.globalFunctions.loading.dismiss();
          this.globalFunctions.presentToast('Post successful.');
          this.navCtrl.push(VehiclePostConfirmPage, {vehDesc: this.vehDesc});
        }
      ); //eo subscribe
      */
  }

  doPush() {
    this.navCtrl.push(VehiclePostConfirmPage);
  }

  ionViewDidLoad() {
  }

}
