import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehiclePostCreatePage } from './vehicle-post-create';

@NgModule({
  declarations: [
    VehiclePostCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(VehiclePostCreatePage),
  ],
})
export class VehiclePostCreatePageModule {}
