import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ListingsDataProvider } from './../../providers/data/listings-data';


@IonicPage()
@Component({
  selector: 'page-manage-listing',
  templateUrl: 'manage-listing.html',
})
export class ManageListingPage {

  selectedListing: any;

  listingStatus: string = "active"

  constructor(public navCtrl: NavController, public navParams: NavParams, public listingDataProvider: ListingsDataProvider) {
    this.loadSelectedListing();
  }

  loadSelectedListing() {
    this.listingDataProvider.selectedListing.subscribe(data => {
      this.selectedListing = data;
    });
  }


}
