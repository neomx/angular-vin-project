import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManageListingPage } from './manage-listing';

@NgModule({
  declarations: [
    ManageListingPage,
  ],
  imports: [
    IonicPageModule.forChild(ManageListingPage),
  ],
})
export class ManageListingPageModule {}
