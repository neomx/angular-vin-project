import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {InfoCastPage} from "../info-cast/info-cast";
import { ProgressBarModule } from "../progress-bar/progress-bar.module";

@IonicPage()
@Component({
  selector: 'page-vehicle-post-confirm',
  templateUrl: 'vehicle-post-confirm.html',
})
export class VehiclePostConfirmPage {
  vehDesc: any;

  progressBarWidth: number = 100;
  startBackgroundColor: string = "#43B7E8";
  iconColor: string = "#43B7E8";
  startBorderColor: string = "2px solid #43B7E8";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.vehDesc = navParams.get('vehDesc');
  }

  ionViewDidLoad() {

  }

  doPush() {
    this.navCtrl.push(InfoCastPage);
  }

}
