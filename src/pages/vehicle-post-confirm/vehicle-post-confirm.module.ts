import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehiclePostConfirmPage } from './vehicle-post-confirm';

@NgModule({
  declarations: [
    VehiclePostConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(VehiclePostConfirmPage),
  ],
})
export class VehiclePostConfirmPageModule {}
