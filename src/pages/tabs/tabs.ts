import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { App } from 'ionic-angular';
import { HomePage } from '../home/home';
import { OffersPage } from '../offers/offers';
import { VehiclesPage } from '../vehicles/vehicles';
import { UserProfilePage } from '../user-profile/user-profile';
import { InfoCastPage } from '../info-cast/info-cast';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  homePage = HomePage;
  offersPage = OffersPage;
  vehiclesPage = VehiclesPage;
  userProfilePage = UserProfilePage;
  infoCastPage = InfoCastPage;
  public tabIndex:Number = 0;

  constructor(public params:NavParams) {
    let tabIndex = this.params.get('tabIndex');
    if(tabIndex){
      this.tabIndex = tabIndex;
    }
  }

}
