import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HowToScanPage } from './how-to-scan';

@NgModule({
  declarations: [
    HowToScanPage,
  ],
  imports: [
    IonicPageModule.forChild(HowToScanPage),
  ],
})
export class HowToScanPageModule {}
