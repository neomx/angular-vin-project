import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfferDeclinedPage } from './offer-declined';

@NgModule({
  declarations: [
    OfferDeclinedPage,
  ],
  imports: [
    IonicPageModule.forChild(OfferDeclinedPage),
  ],
})
export class OfferDeclinedPageModule {}
