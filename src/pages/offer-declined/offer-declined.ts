import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { OfferDetailPage } from "../offer-detail/offer-detail";
import { OffersDataProvider } from '../../providers/data/offers-data';

@IonicPage()
@Component({
  selector: 'page-offer-declined',
  templateUrl: 'offer-declined.html',
})
export class OfferDeclinedPage {

  selectedOffer: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public offersDataProvider: OffersDataProvider,) {
    this.loadSelectedDetails();
  }

  loadSelectedDetails() {
    this.offersDataProvider.selectedOffer.subscribe(data => {
      this.selectedOffer = data;
    });
  }

  doPush() {
    this.navCtrl.push(OfferDetailPage);
  }

}
