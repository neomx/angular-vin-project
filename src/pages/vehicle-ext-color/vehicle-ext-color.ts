import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';
import { GlobalFunctions } from '../../providers/global-functions';
import { GlobalVars } from '../../providers/global-vars';
import { VehicleIntColorPage } from '../../pages/vehicle-int-color/vehicle-int-color';
import { ProgressBarModule } from "../progress-bar/progress-bar.module";

@IonicPage()
@Component({
  selector: 'page-vehicle-ext-color',
  templateUrl: 'vehicle-ext-color.html',
})
export class VehicleExtColorPage {
  vehTrim: any;
  vehVIN: any;
  vehDesc: any;
  private api: any;
  extColorsResult: any;
  extColorsAry: any[];
  intColorsResult: any;
  intColorsAry: any[];
  private count: any;
  private jsonText: any;
  equipData = { 'equipment': [] };
  private equipCount: any;
  noExtColors: boolean;
  noIntColors: boolean;
  noOptions: boolean;
  public clientVehObj: any;

  progressBarWidth: number = 12;
  startBackgroundColor: string = "#43B7E8";
  startBorderColor: string = "2px solid #43B7E8";

  constructor(public navCtrl: NavController, public navParams: NavParams, private consumerAPIProvider: ConsumerAPIProvider, public globalFunctions: GlobalFunctions, private globalVars: GlobalVars) {
    this.vehTrim = navParams.get('vehTrim');
    this.vehDesc = navParams.get('vehDesc');
    this.vehVIN = navParams.get('vehVIN');
  }

  ngOnInit() {
    this.noExtColors = false;
    this.noIntColors = false;
    this.noOptions = false;
    this.globalVars.clientVehicleObj = this.globalVars.storage3;
    this.clientVehObj = this.globalVars.storage1;
    this.doDecodeTrim();
  }

  ngOnDestroy(){
    this.api.unsubscribe();
  }

  ionViewDidLoad() {
  }

  getColorCode(code) {
    return "#" + code;
  }

  doDecodeTrim(){
    this.globalFunctions.showLoader('Decoding...');
    this.api = this.consumerAPIProvider
      .doGet('/decode/'+this.vehVIN+'/'+this.vehTrim)
      .subscribe(
        result => {
          this.globalFunctions.loading.dismiss();
          this.extColorsResult = JSON.stringify(result);
          let jsonText = JSON.parse(this.extColorsResult);

          //get exterior colors to array
          this.count = jsonText.vinDecode.exteriorColors.length;
          if (this.count > 0)
          {
            this.extColorsAry = [jsonText.vinDecode.exteriorColors[0]];
            for (var i = 1; i<this.count; i++){
              this.extColorsAry.push(jsonText.vinDecode.exteriorColors[i]);
            }
          }
          else
          {
            this.noExtColors = true;
          }
         
          //get interior colors to array
          this.count = jsonText.vinDecode.interiorColors.length;
          if (this.count > 0)
          {
            this.intColorsAry = [jsonText.vinDecode.interiorColors[0]];
            for (var i = 1; i<this.count; i++){
              this.intColorsAry.push(jsonText.vinDecode.interiorColors[i]);
            }
          }
          else
          {
            this.noIntColors = true;
          }
          //console.log('intcolors:'+JSON.stringify(this.intColorsAry));
          
          //get options to array
          this.count = jsonText.vinDecode.equipment.length;
          if (this.count > 0)
          {
            for (var i = 0; i<this.count; i++){
              this.equipData.equipment.push({'id': jsonText.vinDecode.equipment[i].id,'value': jsonText.vinDecode.equipment[i].value});
            }
          }
          else
          {
            this.noOptions = true;
          }
        }, //eo result
        err => {
          this.globalFunctions.loading.dismiss();
          this.globalFunctions.presentToast(err);
        }, //eo err
        () => {
          this.api.unsubscribe();
        } //eo ()
      ); //eo subscribe
  };

  doPush(selected: any){
    this.globalVars.storage = this.intColorsAry;
    this.globalVars.storage2 = this.equipData.equipment;
    this.globalVars.clientVehicleObj.colorExterior.push(selected);
    this.globalVars.storage3 = this.globalVars.clientVehicleObj;

    this.clientVehObj.colorExterior = selected.name;
    this.globalVars.storage1 = this.clientVehObj;

    this.navCtrl.push(VehicleIntColorPage, {vehDesc: this.vehDesc, noIntColorsFlag: this.noIntColors, noOptionsFlag: this.noOptions});
  }

}
