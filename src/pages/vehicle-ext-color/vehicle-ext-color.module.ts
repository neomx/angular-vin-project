import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleExtColorPage } from './vehicle-ext-color';

@NgModule({
  declarations: [
    VehicleExtColorPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleExtColorPage),
  ],
})
export class VehicleExtColorPageModule {}
