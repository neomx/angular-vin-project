import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ScanPage } from '../scan/scan';
import { HowToScanPage } from '../how-to-scan/how-to-scan';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  scanPage = ScanPage;
  howToScanPage = HowToScanPage;
  
  constructor(public navCtrl: NavController) {
  }

}