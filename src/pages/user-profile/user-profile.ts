import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { App } from 'ionic-angular';
import { UsernameValidator } from '../../validators/username.validator';
import { PasswordValidator } from '../../validators/password.validator';
import { LaunchPage } from '../launch/launch';
import { HomePage } from '../home/home';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';
import { SigninPage } from '../signin/signin';
import { GlobalVars } from '../../providers/global-vars';
import { GlobalFunctions } from '../../providers/global-functions';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {

  launchPage = LaunchPage;
  validations_form: FormGroup;
  matching_passwords_group: FormGroup;
  prefContact = 'prefContact';
  data: any;
  private api: any;

  settings = {
    enableNotifications: true,
    receiveOffers: false,

  };

  apps: any = {
    'Email' : [{
      name: 'Email'
    }],
    'CellPhone' : [{
      name: 'CellPhone'
    }],
    'HomePHone' : [{
      name: 'HomePhone'
    }]
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private app:App, public consumerAPIProvider: ConsumerAPIProvider, public globalVars: GlobalVars, public globalFunctions: GlobalFunctions) {
  }

  ngOnInit(){
    this.prefContact = "Email"; /*sets email as the default segment button*/
  }

  ionViewWillLoad() {
    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    }); /*eo FormGroup*/
    
    this.validations_form = this.formBuilder.group({
      name: new FormControl('', Validators.compose([
        UsernameValidator.validUsername,
        Validators.maxLength(25),
        Validators.minLength(5),
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      matching_passwords: this.matching_passwords_group,
      cellphone: new FormControl('', Validators.compose([
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      homephone: new FormControl('', Validators.compose([
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      zip: new FormControl('', Validators.compose([
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ]))

    }); /*eo formBuilder group*/
  } /*eo ionViewWillLoad*/

  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required.' },
      { type: 'minlength', message: 'Name must be at least 5 characters long.' },
      { type: 'maxlength', message: 'Name cannot be more than 50 characters long.' }
      /*{ type: 'pattern', message: 'Your username must contain only numbers and letters.' },
      { type: 'validUsername', message: 'Your username has already been taken.' }*/
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
      /*{ type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number.' }*/
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirm password is required' }
    ],
    'matching_passwords': [
      { type: 'areEqual', message: 'Password mismatch' }
    ]
  };

  submitLogin() 
  {
    console.log('Doing login..');
  }

  notify(){
    console.log("toggled");
  }

  selectedEmail(){
    console.log("email selected");
  };

  selectedCellPhone(){
    console.log("cell phone selected");
  };

  selectedHomePhone(){
    console.log("home phone selected");
  };
  
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad UserProfilePage');
  }

  onSubmit(values){
    /*this.navCtrl.push(UserPage);*/
    console.log("form submit");
  }

  doSignOut(){
    /*logout() end-point clears the cookie and returns a null object*/
    this.api = this.consumerAPIProvider
      .doGet('/user/logout')
      .subscribe(
        result => {
        },
        err => {
          this.globalFunctions.presentToast(err);
        },
        () => {
          //this.api.unsubscribe();
          //this method removes the tabs prior to setting the root page to LaunchPage
          let nav = this.app.getRootNav();
          nav.setRoot(LaunchPage);
        }
      )
  }

  doSaveInfo(){
    this.globalFunctions.showLoader('Saving your profile information...');
    this.globalFunctions.loading.dismiss();
    this.globalFunctions.presentToast('Saved successfully');
  }
  
  ngOnDestroy() {
    //unsubscribe to ensure no memory leaks
    this.api.unsubscribe();
  }

}
