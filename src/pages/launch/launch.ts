import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { App } from 'ionic-angular';
import { SigninPage } from '../signin/signin';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { UserProvider } from '../../providers/user/user';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';
import { GlobalVars } from '../../providers/global-vars';

@IonicPage()
@Component({
  selector: 'page-launch',
  templateUrl: 'launch.html',
})
export class LaunchPage {
  isGlobalActive: boolean;
  signinPage = SigninPage;
  signupPage = SignupPage;
  homePage = HomePage;

  private api: any;
  private status: boolean;
  private statusResponse: any = {
    active: false,
    status: {
      active: false
    }
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider,
     private app: App, public globalVars: GlobalVars, public consumerAPIProvider: ConsumerAPIProvider) {
    this.getSignInStatus();
  }

  getSignInStatus()
  {
    this.status = false;
    this.api = this.consumerAPIProvider
      .doGet('/user/status')
      .subscribe(
        result => {
          this.statusResponse = result;
        },
        err => {
          //console.log(err);
        },
        () => {
          if (this.statusResponse.status.active) {
            this.navCtrl.setRoot(TabsPage, {tabIndex: 1});
          }
        }
      );
  }

}
