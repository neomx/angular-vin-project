import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { OffersDataProvider } from '../../providers/data/offers-data';

import { OfferDeclinedPage } from './../offer-declined/offer-declined';
import { OfferAcceptedPage } from './../offer-accepted/offer-accepted';

@IonicPage()
@Component({
  selector: 'page-offer-detail',
  templateUrl: 'offer-detail.html',
})
export class OfferDetailPage {

  selectedOffer: any;

  offerAcceptedPage = OfferAcceptedPage;
  offerDeclinedPage = OfferDeclinedPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public offersDataProvider: OffersDataProvider,) {
    this.loadSelectedDetails();
  }

  loadSelectedDetails() {
    this.offersDataProvider.selectedOffer.subscribe(data => {
      this.selectedOffer = data;
    });
  }

}
