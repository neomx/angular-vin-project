import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoScanPage } from './no-scan';

@NgModule({
  declarations: [
    NoScanPage,
  ],
  imports: [
    IonicPageModule.forChild(NoScanPage),
  ],
})
export class NoScanPageModule {}
