import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ScanPage } from '../../pages/scan/scan';
import { GlobalFunctions } from '../../providers/global-functions';
import { GlobalVars } from '../../providers/global-vars';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';
import { VehicleTrimPage } from '../../pages/vehicle-trim/vehicle-trim';

@IonicPage()
@Component({
  selector: 'page-no-scan',
  templateUrl: 'no-scan.html',
})
export class NoScanPage implements OnInit, OnDestroy {
  scanPage = ScanPage;
  vehicleTrimPage = VehicleTrimPage;
  vinData = {vin:'2HGFG4A53FH706957'};
  //vinData = {vin:''};
  private api: any;
  apiResult: any;
  private count: any;
  trimData = { 'trims': [] };
  public clientVehObj: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalFunctions: GlobalFunctions,
     private consumerAPIProvider: ConsumerAPIProvider, private globalVars: GlobalVars) {
  }

  ngOnInit(){
    this.clientVehObj = this.globalVars.storage1;
  }

  ngOnDestroy(){
    this.api.unsubscribe();
  }

  doDecodeVIN(){
    let vehID;
    let vehYear = '';
    let vehMake = '';
    let vehModel = '';
    let vehVIN = '';
    let vehDesc = '';
    let vehBody = '';
    let vehDriveTrain = '';
    this.globalFunctions.showLoader('Decoding...');
    let vin = this.vinData.vin;

    this.api = this.consumerAPIProvider
      .doGet('/decode/'+vin)
      .subscribe(
        result => {
          this.apiResult = JSON.stringify(result);
          let jsonText = JSON.parse(this.apiResult);
          this.count = jsonText.vinDecode.trims.length;
          for (var i = 0; i<this.count; i++){
            this.trimData.trims.push({'id': jsonText.vinDecode.trims[i].id,'name': jsonText.vinDecode.trims[i].name});
          }

          let jsonKeyVal = JSON.parse(this.apiResult, (key, value) => {
            //console.log('keyval:' + key+':'+value);
            if (key === 'vehicleId')
            {
              vehID = value;
            }
            if (key === 'year')
            {
              vehYear = value;
            }
            if (key === 'make')
            {
              vehMake =  value;
            }
            if (key === 'model')
            {
              vehModel = value;
            }
            if (key === 'vin')
            {
              vehVIN = value;
            }
            if (key === 'body')
            {
              vehBody = value;
            }
            if (key === 'drivetrain')
            {
              vehDriveTrain = value;
            }
          }
        )},
        err => {
          //no need to call unsubscribe since an err triggers an unsubscribe automatically
          this.globalFunctions.loading.dismiss();
          this.globalFunctions.presentToast(err);
        },
        () => {
          this.api.unsubscribe();
          this.globalFunctions.loading.dismiss();
          this.globalFunctions.presentToast('Decoding completed.');
          if (this.count > 0)
          {
            this.globalVars.clientVehicleObj.vin.push(vehVIN);
            this.globalVars.clientVehicleObj.year.push(vehYear);
            this.globalVars.clientVehicleObj.make.push(vehMake);
            this.globalVars.clientVehicleObj.model.push(vehModel);
            this.globalVars.clientVehicleObj.bodyType.push(vehBody);
            this.globalVars.clientVehicleObj.drivetrain.push(vehDriveTrain);
            this.globalVars.storage3 = this.globalVars.clientVehicleObj;

            this.globalVars.storage = this.trimData.trims;
            vehDesc = vehYear+' '+vehMake+' '+vehModel;

            this.clientVehObj.extras.vehicleId = vehID;
            this.clientVehObj.vin = vehVIN;
            this.clientVehObj.year = vehYear;
            this.clientVehObj.make = vehMake;
            this.clientVehObj.model = vehModel;
            this.clientVehObj.vin = vehBody;
            this.clientVehObj.drivetrain= vehDriveTrain;
            this.clientVehObj.body= vehBody;
            this.globalVars.storage1 = this.clientVehObj;

            this.navCtrl.push(VehicleTrimPage, {vehVIN: vehVIN, vehDesc: vehDesc});
          }
          else
          {
            this.globalFunctions.presentToast('No VIN data was found.');
          }
        }
      );
  }

  ionViewDidLoad() {
  }
}
