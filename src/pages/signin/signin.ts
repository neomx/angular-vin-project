import { Component, OnDestroy } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { UserProvider } from '../../providers/user/user';
import { ConsumerAPIProvider } from '../../providers/api/consumer-api';
import { TabsPage } from '../tabs/tabs';
import { ResetPasswordPage } from '../reset-password/reset-password';
import { InfoCastPage } from '../info-cast/info-cast';
import { GlobalVars } from '../../providers/global-vars';
import { GlobalFunctions } from '../../providers/global-functions';
import { VehiclePhotosPage } from '../vehicle-photos/vehicle-photos';

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {
  resetPasswordPage = ResetPasswordPage;
  infoCastPage = InfoCastPage;
  loading: any;
  signinData = { username:'', password:'' };
  data: any;
  infocast: any;
  objectCount: any;
  infoCastHasData: boolean = false;
  private api: any;
  apiResult: any;
  public clientVehObj;

  constructor(public navCtrl: NavController, public userProvider: UserProvider, public loadingCtrl: LoadingController,
    public device: Device, public globalVars: GlobalVars, public consumerAPIProvider: ConsumerAPIProvider, public globalFunctions: GlobalFunctions) {
      this.getInfoCast();

      //this.clientVehObj = this.globalVars.clientVehicleObj2;
  }

  deviceData = {
    deviceInfo: {
      model: this.device.model,
      platform: this.device.platform,
      uuid: this.device.uuid,
      version: this.device.version,
      manufacturer: this.device.manufacturer,
      isVirtual: true,
      serial: this.device.serial,
      ip: '[:IP:]'
    }
  };

  getInfoCast(){
    this.userProvider.getInfoCast()
    .then (data => {
      this.infocast = data;
      this.objectCount = this.infocast.length;
      if ( this.infocast.length > 0)
      {
        this.infoCastHasData = true;
      }
      else
      {
        this.infoCastHasData = false;
      }
    })
  }

  doSignIn(){
    let clientId = '';
    this.globalFunctions.showLoader('Authenticating...');
    this.api = this.consumerAPIProvider
      .doPost('/user/login', JSON.stringify(this.signinData) + ',' + JSON.stringify(this.deviceData))
      .subscribe(
        result => {
          this.apiResult = JSON.stringify(result);
          let jsonText = JSON.parse(this.apiResult);
          this.globalVars.clientVehicleObj.clientId.push(jsonText.client.id);
          this.globalVars.storage3 = this.globalVars.clientVehicleObj;
          
          //building global client vehicle data object
          this.globalVars.clientVehicleObj2.clientId = jsonText.client.id;
          this.globalVars.clientVehicleObj2.client.contactId = (jsonText.client.contactId);
          this.globalVars.clientVehicleObj2.client.contact.name = (jsonText.client.contact.name);
          this.globalVars.clientVehicleObj2.client.contact.lastName = (jsonText.client.contact.lastName);
          this.globalVars.storage1 = this.globalVars.clientVehicleObj2;

        },
        err => {
          //console.log(err);
          this.globalFunctions.loading.dismiss();
          this.globalFunctions.presentToast('Invalid signin.  Please try again or create an account.');
        },
        () => {
          this.globalFunctions.loading.dismiss();
          if (this.infoCastHasData)
          {
            this.navCtrl.setRoot(TabsPage, {tabIndex: 1});
          }
          else
          {
            //send to a tab page if authentication is successful
            //tabIndex property sets the tab page to nagivate to. first tab page index starts at 0
            this.navCtrl.setRoot(TabsPage, {tabIndex: 0});
          }
        }
      )
  }

  ngOnDestroy() {
    //unsubscribe to ensure no memory leaks
    //this.api.unsubscribe();
  }

}
