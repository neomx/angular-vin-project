import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleListingPage } from './vehicle-listing';

@NgModule({
  declarations: [
    VehicleListingPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleListingPage),
  ],
})
export class VehicleListingPageModule {}
