import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { SigninPage } from '../signin/signin';

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {

  signInPage = SigninPage;
  userEmailData = { useremail:''};
  loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
  }

  doReset() {
    /*this.showLoader();
    this.loading.dismiss();*/
    this.presentToast('An email has been sent to your account with a link to reset your password.');
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Reseting password...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      /*duration: 3000,*/
      position: 'middle',
      dismissOnPageChange: true,
      showCloseButton: true,
      closeButtonText: 'Close'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
       this.navCtrl.setRoot(SigninPage);
    });

    toast.present();
  }
  
  /*
  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }
  */
}
