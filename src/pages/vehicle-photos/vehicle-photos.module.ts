import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehiclePhotosPage } from './vehicle-photos';

@NgModule({
  declarations: [
    VehiclePhotosPage,
  ],
  imports: [
    IonicPageModule.forChild(VehiclePhotosPage),
  ],
})
export class VehiclePhotosPageModule {}
