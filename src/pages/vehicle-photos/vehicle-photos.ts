import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { DomSanitizer } from '@angular/platform-browser';
import { VehicleConditionsPage } from '../../pages/vehicle-conditions/vehicle-conditions';
import { ProgressBarModule } from "../progress-bar/progress-bar.module";
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { GlobalVars } from '../../providers/global-vars';

@IonicPage()
@Component({
  selector: 'page-vehicle-photos',
  templateUrl: 'vehicle-photos.html',
})
export class VehiclePhotosPage {
  public cameraData: string;
  photoTaken: boolean;
  photoSelected: boolean;
  images: any = [];
  selectedImageIndex: number;
  vehDesc: any;
  lastImage: string = null;
  progressBarWidth: number = 30;
  startBackgroundColor: string = "#43B7E8";
  startBorderColor: string = "2px solid #43B7E8";
  iconColor: string = "#43B7E8";

  constructor(public navCtrl: NavController, public navParams: NavParams, private imagePicker: ImagePicker,
     public DomSanitizer: DomSanitizer, public camera: Camera, private globalVars: GlobalVars) {
    this.vehDesc = navParams.get('vehDesc');
  }

  ngOnInit(){
    this.photoTaken = false;
  }

  getPictures(){
    let options = {
      maximumImagesCount: 5,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 100,
      targetHeight: 100,
      correctOrientation: true,
      allowEdit: false,
      quality: 100
    }
    this.images = new Array<string>();
    this.imagePicker.getPictures(options)
      .then((result) => {
        this.selectedImageIndex = 0;
        for (let i=0;i < result.length;i++) {
          this.images.push(result[i]);
          //console.log('image select:'+result[i]);
        }
        //console.log('images count:'+this.images.length);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  removePicture() {
    this.images.splice(this.selectedImageIndex, 1);
    this.selectedImageIndex = 0;
  }

  selectImage(index) {
    this.selectedImageIndex = index;
  }

  openCamera() {
    var options = {
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: true,
      targetWidth: 1000,
      targetHeight: 1000,
      correctOrientation: true,
      allowEdit: false,
      quality: 100
    };
    this.camera.getPicture(options).then((imageData) => {
      this.cameraData = 'data:image/jpeg;base64,' + imageData;
      this.photoTaken = true;
      this.photoSelected = false;
    }, (err) => {
      // Handle error
    });
  }

  doPush(){
    this.navCtrl.push(VehicleConditionsPage, {vehDesc: this.vehDesc});
  }

  ionViewDidLoad() {
  }

}
