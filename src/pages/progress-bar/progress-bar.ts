import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-progress-bar',
  templateUrl: 'progress-bar.html',
})
export class ProgressBarPage {

  @Input() barWidth: number;
  @Input() startBorderColor: string;
  @Input() describeBorderColor: string;
  @Input() describeIconColor: string;
  @Input() priceBorderColor: string;
  @Input() priceIconColor: string;
  @Input() postBorderColor: string;
  @Input() postIconColor: string;
  @Input() startCircleBackground: string;
  @Input() startCircleBorder: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
