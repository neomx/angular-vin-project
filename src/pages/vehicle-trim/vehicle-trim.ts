import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalVars } from '../../providers/global-vars';
import { VehicleExtColorPage } from '../../pages/vehicle-ext-color/vehicle-ext-color';
import { ProgressBarModule } from "../progress-bar/progress-bar.module";


@IonicPage()
@Component({
  selector: 'page-vehicle-trim',
  templateUrl: 'vehicle-trim.html',
})
export class VehicleTrimPage {
  trimData: any;
  vehDesc: any;
  vehVIN: any;
  public clientVehObj: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private globalVars: GlobalVars) {
    this.vehDesc = navParams.get('vehDesc');
    this.vehVIN = navParams.get('vehVIN');
  }

  ngOnInit(){
    //console.log("ngOnInit called");
    this.trimData = this.globalVars.storage;
    this.globalVars.clientVehicleObj = this.globalVars.storage3;
    this.clientVehObj = this.globalVars.storage1;
  }

  doPush(selected: any){
    this.globalVars.clientVehicleObj.trim.push(selected.name);
    this.globalVars.storage3 = this.globalVars.clientVehicleObj;

    this.clientVehObj.trim = selected.name;
    this.globalVars.storage1 = this.clientVehObj;

    this.navCtrl.push(VehicleExtColorPage, {vehVIN: this.vehVIN, vehDesc: this.vehDesc, vehTrim: selected.id});
  }

}
