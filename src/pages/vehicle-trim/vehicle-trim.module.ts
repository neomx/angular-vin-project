import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleTrimPage } from './vehicle-trim';

@NgModule({
  declarations: [
    VehicleTrimPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleTrimPage),
  ],
})
export class VehicleTrimPageModule {}
