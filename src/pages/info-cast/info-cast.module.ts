import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfoCastPage } from './info-cast';

@NgModule({
  declarations: [
    InfoCastPage,
  ],
  imports: [
    IonicPageModule.forChild(InfoCastPage),
  ],
})
export class InfoCastPageModule {}
