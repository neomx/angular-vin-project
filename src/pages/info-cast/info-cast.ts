import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-info-cast',
  templateUrl: 'info-cast.html',
})
export class InfoCastPage {
  products: any;
  infocast: any;
  noDataPage: boolean = false;
  //public clientVehObj: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider) {
    this.getInfoCast();
    //this.clientVehObj = this.globalVars.storage1;
  }

  getProducts(){
    this.userProvider.getProducts()
    .then (data => {
      this.products = data;
    })
  }

  getInfoCast(){
    this.userProvider.getInfoCast()
    .then (data => {
      this.infocast = data ? data : null;
      this.noDataPage = data.length === 0 ? true : false;
    })
  }
}
