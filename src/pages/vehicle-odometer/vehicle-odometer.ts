import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VehiclePhotosPage } from '../../pages/vehicle-photos/vehicle-photos';
import { GlobalVars } from '../../providers/global-vars';

@IonicPage()
@Component({
  selector: 'page-vehicle-odometer',
  templateUrl: 'vehicle-odometer.html',
})
export class VehicleOdometerPage {
  vehDesc: any;
  odometerValue: number;
  selectedOptions: any;
  public clientVehObj: any;
  progressBarWidth: number = 22;
  startBackgroundColor: string = "#43B7E8";
  startBorderColor: string = "2px solid #43B7E8";

  public odometerForm = this.fb.group({
    odometer: ['', Validators.required],
  });

  constructor(public navCtrl: NavController, public navParams: NavParams, public fb: FormBuilder, private globalVars: GlobalVars) {
    this.vehDesc = navParams.get('vehDesc');
    this.selectedOptions = navParams.get('selectedOptions');
  }

  ngOnInit(){
    this.clientVehObj = this.globalVars.storage1;
  }

  ionViewDidLoad() {
  }

  doPush(){
    this.odometerValue = this.odometerForm.value;
    this.clientVehObj.odometer = this.odometerValue;
    this.globalVars.storage1 = this.clientVehObj;
    this.navCtrl.push(VehiclePhotosPage, {vehDesc: this.vehDesc});
  }

}
