import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleOdometerPage } from './vehicle-odometer';

@NgModule({
  declarations: [
    VehicleOdometerPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleOdometerPage),
  ],
})
export class VehicleOdometerPageModule {}
