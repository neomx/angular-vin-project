import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { OffersDataProvider } from '../../providers/data/offers-data';

@IonicPage()
@Component({
  selector: 'page-offer-accepted',
  templateUrl: 'offer-accepted.html',
})
export class OfferAcceptedPage {

  selectedOffer: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public offersDataProvider: OffersDataProvider,) {
    this.loadSelectedDetails();
  }

  loadSelectedDetails() {
    this.offersDataProvider.selectedOffer.subscribe(data => {
      this.selectedOffer = data;
    });
  }

}
