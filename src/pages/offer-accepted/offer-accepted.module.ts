import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfferAcceptedPage } from './offer-accepted';

@NgModule({
  declarations: [
    OfferAcceptedPage,
  ],
  imports: [
    IonicPageModule.forChild(OfferAcceptedPage),
  ],
})
export class OfferAcceptedPageModule {}
