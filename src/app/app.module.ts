import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CookieInterceptor } from '../providers/cookie-interceptor';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Toast } from '@ionic-native/toast';
import { Device } from '@ionic-native/device';
import { GlobalVars } from '../providers/global-vars';
import { GlobalFunctions } from '../providers/global-functions';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { LaunchPage } from '../pages/launch/launch';
import { HomePage } from '../pages/home/home';
import { SigninPage } from '../pages/signin/signin';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { UserProvider } from '../providers/user/user';
import { OffersDataProvider } from '../providers/data/offers-data';
import { ListingsDataProvider } from './../providers/data/listings-data';
import { ConsumerAPIProvider } from '../providers/api/consumer-api';
import { Http } from '@angular/http/src/http';
import { OffersPage } from '../pages/offers/offers';
import { VehiclesPage } from '../pages/vehicles/vehicles';
import { TabsPage } from '../pages/tabs/tabs';
import { ScanPage } from '../pages/scan/scan';
import { HowToScanPage } from '../pages/how-to-scan/how-to-scan';
import { NoScanPage } from '../pages/no-scan/no-scan';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { UserProfilePage } from '../pages/user-profile/user-profile';
import { InfoCastPage } from '../pages/info-cast/info-cast';
import { ProgressBarPage } from '../pages/progress-bar/progress-bar';
import { OfferDetailPage } from '../pages/offer-detail/offer-detail';
import { OfferAcceptedPage } from '../pages/offer-accepted/offer-accepted';
import { OfferDeclinedPage } from '../pages/offer-declined/offer-declined';
import { VehicleTrimPage } from '../pages/vehicle-trim/vehicle-trim';
import { VehicleExtColorPage } from '../pages/vehicle-ext-color/vehicle-ext-color';
import { VehicleIntColorPage } from '../pages/vehicle-int-color/vehicle-int-color';
import { ManageListingPage } from '../pages/manage-listing/manage-listing';
import { VehicleListingPage } from '../pages/vehicle-listing/vehicle-listing';
import { VehicleOptionsPage } from '../pages/vehicle-options/vehicle-options';
import { VehiclePhotosPage } from '../pages/vehicle-photos/vehicle-photos';
import { VehicleOdometerPage } from '../pages/vehicle-odometer/vehicle-odometer';
import { VehicleConditionsPage } from '../pages/vehicle-conditions/vehicle-conditions';
import { VehiclePricingPage } from '../pages/vehicle-pricing/vehicle-pricing';
import { VehiclePostCreatePage } from '../pages/vehicle-post-create/vehicle-post-create';
import { VehiclePostConfirmPage } from '../pages/vehicle-post-confirm/vehicle-post-confirm';


@NgModule({
  declarations: [
    MyApp,
    LaunchPage,
    HomePage,
    SigninPage,
    LoginPage,
    SignupPage,
    OffersPage,
    VehiclesPage,
    TabsPage,
    ScanPage,
    HowToScanPage,
    NoScanPage,
    ResetPasswordPage,
    UserProfilePage,
    InfoCastPage,
    ProgressBarPage,
    OfferDetailPage,
    OfferAcceptedPage,
    OfferDeclinedPage,
    VehicleTrimPage,
    VehicleExtColorPage,
    VehicleIntColorPage,
    ManageListingPage,
    VehicleListingPage,
    VehicleOptionsPage,
    VehiclePhotosPage,
    VehicleOdometerPage,
    VehicleConditionsPage,
    VehiclePricingPage,
    VehiclePostCreatePage,
    VehiclePostConfirmPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      scrollAssist: false,
      autoFocusAssist: false
    }),
    HttpModule,
    IonicImageViewerModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LaunchPage,
    HomePage,
    SigninPage,
    LoginPage,
    SignupPage,
    OffersPage,
    VehiclesPage,
    TabsPage,
    ScanPage,
    HowToScanPage,
    NoScanPage,
    ResetPasswordPage,
    UserProfilePage,
    InfoCastPage,
    ProgressBarPage,
    OfferDetailPage,
    OfferAcceptedPage,
    OfferDeclinedPage,
    VehicleTrimPage,
    VehicleExtColorPage,
    VehicleIntColorPage,
    ManageListingPage,
    VehicleListingPage,
    VehicleOptionsPage,
    VehiclePhotosPage,
    VehicleOdometerPage,
    VehicleConditionsPage,
    VehiclePricingPage,
    VehiclePostCreatePage,
    VehiclePostConfirmPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    BarcodeScanner,
    Toast,
    Device,
    GlobalVars,
    GlobalFunctions,
    OffersDataProvider,
    ListingsDataProvider,
    ConsumerAPIProvider,
    ScreenOrientation,
    Camera,
    ImagePicker,
    {provide: HTTP_INTERCEPTORS, useClass: CookieInterceptor, multi: true}
  ]
})
export class AppModule {}
