import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Storage } from '@ionic/storage';

import { Observable } from 'rxjs/Observable';
import {map} from 'rxjs/operators';
import 'rxjs/Rx';

@Injectable()
export class CookieInterceptor implements HttpInterceptor {

  constructor(private storage: Storage) {
  }

  private cookie: string = undefined;
  private cookieSet: boolean = false;

  private getCookie (): Observable<any> {
    return new Observable<any>(observer => {
      if (!this.cookieSet) {
        this.storage.get('cookie').then((val) => {
          if (val != null) {
            this.cookie = val;
            this.cookieSet = true;
            observer.next (this.cookie);
          } else {
            observer.next ('');
          }
          observer.complete ();
        }, (err) => {
          observer.error (err);
        });
      } else {
        observer.next (this.cookie);
        observer.complete ();
      }
    });
  }

  private setCookie (c: string) {
    this.cookie = c;
    this.storage.set('cookie', c).then(() => {
      //console.log ('cookie set');
    }, (err) => {
      //console.log ('cookie not set');
    });
  }

  private checkHeaders (res: HttpResponse<any>) {
    if (res.headers) {
      //console.log (res.headers.keys ());
      let cookie = res.headers.get ('VV-API-Set-Cookie');
      if (cookie != null && cookie != '') {
        //console.log (cookie);
        this.setCookie (cookie);
      }
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    return this.getCookie().flatMap((cookE) => {
      return next.handle(req.clone({
        headers: req.headers.set ('VV-API-Cookie', cookE)
      })).pipe (
        map((res: any) => {
          this.checkHeaders (res);
          return res;
        }),
        res => {
          res.subscribe (
            result => {
              //console.log ('result');
              //console.log (result);
              //this.checkHeaders (result);
            },
            error => {
              //console.log ('error');
              //console.log (error);
              this.checkHeaders (error);
            },
            () => {
            }
          );
          return res;
        }
      );
    });
  }

}
