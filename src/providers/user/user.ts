import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

let apiSigninUrl = 'https://api.vinvoyager.com/v0/consumer/user/login'
let apiSignupUrl = 'https://api.vinvoyager.com/v0/user/client'
let apiSignoutUrl = 'https://api.vinvoyager.com/v0/consumer/user/logout'
let apiUserStatusUrl = 'https://api.vinvoyager.com/v0/consumer/user/status'

@Injectable()
export class UserProvider {
  
  data: any;
  
  constructor(public http: Http) {}

  signin(credentials,deviceInfo) {
    credentials.deviceInfo = {
      model: deviceInfo.model,
      platform: deviceInfo.platform,
      uuid: deviceInfo.uuid,
      version: deviceInfo.version,
      manufacturer: deviceInfo.manufacturer,
      isVirtual: true,
      serial: deviceInfo.serial
    };

    return new Promise((resolve, reject) => {  
      this.http.post(apiSigninUrl, JSON.stringify(credentials), { withCredentials: true })
        .subscribe(res => {
          resolve(res.json());
          }, (err) => {
            reject(err);
      });
    });
  }

  signup(credentials) {
    return new Promise((resolve, reject) => {
      this.http.post(apiSignupUrl, JSON.stringify(credentials), { withCredentials: true })
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
        reject(err);
      });
    });
  }

  signout(){
    return new Promise((resolve, reject) => {
      /*
      let options = new RequestOptions({headers: new Headers(), withCredentials: true});
      let headers = new Headers();    
      headers.append('Accept', 'application/json');
      headers.append('Content-Type', 'application/json');
      */
      this.http.get(apiSignoutUrl, { withCredentials: true })
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
        reject(err);
      });
    });
  }

  getSignInStatus(){
    if (this.data) {
      return Promise.resolve(this.data);
    }
  
    return new Promise(resolve => {
      this.http.get(apiUserStatusUrl, { withCredentials: true })
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  getProducts(){
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http.get('assets/data/products.json')
      .map(res => res.json())
      .subscribe(data => {
        this.data = data;
        resolve(this.data);
      })
    })
  }

  getInfoCast(){
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http.get('assets/data/infocast.json')
      .map(res => res.json())
      .subscribe(data => {
        this.data = data;
        resolve(this.data);
      })
    })
  }

}
