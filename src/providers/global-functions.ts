import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from 'ionic-angular';

@Injectable()

export class GlobalFunctions {
  loading: any;
  constructor(private toastCtrl: ToastController, public loadingCtrl: LoadingController) {}

  showLoader(message){
    this.loading = this.loadingCtrl.create({
        content: message
    });
    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'bottom',
      dismissOnPageChange: true
    });
    
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    
    toast.present();
  }

}