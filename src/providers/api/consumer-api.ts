import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError} from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class ConsumerAPIProvider {

    private baseUrl: string = 'https://api.vinvoyager.com/v0/consumer';

    constructor(public http: HttpClient) { }

    private httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      })
    };

    /* new code section */

    doGet(endPoint: string): Observable<Response> {
      return this.http.get(this.baseUrl + endPoint, this.httpOptions).pipe(
        catchError(this.handleError)
      )
    }

    doPost(endPoint: string, dataObject: Object): Observable<Response> {
      return this.http.post(this.baseUrl + endPoint, dataObject, this.httpOptions).pipe(
        catchError(this.handleError)
      )
    }

    doPut(endPoint: string, dataObject: Object): Observable<Response> {
      return this.http.put(this.baseUrl + endPoint, dataObject, this.httpOptions).pipe(
        catchError(this.handleError)
      )
    }

    doDelete(endPoint: string): Observable<Response> {
      return this.http.put(this.baseUrl + endPoint, this.httpOptions).pipe(
        catchError(this.handleError)
      )
    }

    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
          const err = error || '';
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
          errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}
