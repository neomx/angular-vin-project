import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';

@Injectable()
export class OffersDataProvider {

    data: any;

    selectedOffer = new BehaviorSubject<any>(null);

    constructor (public http: Http) {}

    getMyOffers(){
        if (this.data) {
          return Promise.resolve(this.data);
        }
    
        return new Promise(resolve => {
          this.http.get('assets/data/myoffers.json')
          .map(res => res.json())
          .subscribe(data => {
            this.data = data;
            console.log(data);
            this.selectedOffer.next(data[0]);
            resolve(this.data);
          })
        })
      }

      updateSelectedOffer(offer) {
        this.selectedOffer.next(offer);
      }


}