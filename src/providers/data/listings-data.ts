import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';

@Injectable()
export class ListingsDataProvider {

	data: any;

	selectedListing = new BehaviorSubject<any>(null);


	constructor (public http: Http) {}

	getMyListings(){
			if (this.data) {
				return Promise.resolve(this.data);
			}
	
			return new Promise(resolve => {
				this.http.get('assets/data/mylistings.json')
				.map(res => res.json())
				.subscribe(data => {
					this.data = data;
					this.selectedListing.next(data[0]);
					resolve(this.data);
				})
			})
		}

	updateSelectedListing(listing) {
		this.selectedListing.next(listing);
	}

}