import { Injectable } from '@angular/core';

@Injectable()

export class GlobalVars {
    gActiveVar: boolean;
    gVehDesc: string;

    public storage: any;
    public storage1: any;
    public storage2: any;
    public storage3: any;

    public clientVehicleObj2 = { 
        clientId: '',
            client: {
                settings: {
                    notifications: true,
                    sms: true,
                    contactMethod: 'phone_cell'
                },
                billing: {
                    balance: 0
                },
                security: {
                    username: '',
                    password: '',
                    active: true,
                    tfa: true,
                    tfaSecret: true,
                    resetToken: true,
                    resetDate: ''
                },
                contactId: 0,
                contact:  {
                    title: '',
                    name: '',
                    lastName: '',
                    email: '',
                    phoneCell: '',
                    phoneHome: '',
                    addressId: 0,
                    address: {
                        address: '',
                        address2: '',
                        city: '',
                        postalCode: '',
                        district: '',
                        countryCode: '',
                        country: {
                            continent: {}
                        },
                        latitude: '',
                        longitude:  '',
                    },
                    timezoneId: '',
                    timezone: {},
                    dataValidation: {
                        valid: true
                    }
                },
                languageCode: '',
                language: {},
                status: '',
                active: true,
                referredId : 0,
                inLimbo: true
            }, //eo client
            vin: '',
            extraId: 0,
            extras: {
                vehicleId: 0,
                damageExterior: 0,
                damageInterior: 0,
                damageTires: 0,
                damageSteering: 0,
                damageClimateControl: 0,
                pricing: {
                    retail: 0,
                    clean: 0,
                    average: 0,
                    rough: 0,
                    privateParty: 0,
                    dealerTrade: 0,
                    demand: 0
                },
                odometer: 0,
                superseded: true,
                previousId : 0,
                created: ''
            },
            year: 0,
            make: '',
            model: '',
            trim: '',
            bodyType: '',
            drivetrain: '',
            engine: '',
            transmission: '',
            colorExterior: '',
            colorInterior: '',
            options: [],
            photos: [],
            description: '',
            updated: '',
            created: '',
            active: true,
            deleted: true
    };

    public clientVehicleObj = { 
        'clientId': [],
        'vin': [],
        'year': [],
        'make': [],
        'model': [],
        'trim': [],
        'bodyType': [],
        'drivetrain': [],
        'engine': [],
        'transmission': [],
        'colorExterior': [],
        'colorInterior': []
    };
  
    constructor() {
        this.gActiveVar = false;
    }

    setGlobalActiveVar(value) {
        this.gActiveVar = value;
    }

    getGlobalActiveVar() {
        return this.gActiveVar;
    }
    
    setGlobalVehDescVar(value) {
        this.gVehDesc = value;
    }

    getGlobalVehDescVar() {
        return this.gVehDesc;
    } 

}